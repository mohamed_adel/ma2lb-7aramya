package com.example.game;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.graphics.Rect;

public class SelectLevel extends Activity implements OnClickListener {

	ImageButton six, nine, twelve;
	Board board = new Board();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_select_level);

		six = (ImageButton) findViewById(R.id.six);
		nine = (ImageButton) findViewById(R.id.nine);
		twelve = (ImageButton) findViewById(R.id.twelve);
		
		six.setOnClickListener(this);
		nine.setOnClickListener(this);
		twelve.setOnClickListener(this);	
	}

	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.select_level, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		//Intent intent = new Intent(SelectLevel.this, GamePlay.class);
		switch (v.getId()) {
		case R.id.six:
			board.createBoard(1 , getWindowManager().getDefaultDisplay());
			break;
		case R.id.nine:
			board.createBoard(2 , getWindowManager().getDefaultDisplay());
			break;
		case R.id.twelve :
			board.createBoard(3 , getWindowManager().getDefaultDisplay());
			break;
		}
		//intent.putExtra("board", board);
		//startActivity(intent);
		GameView view = new GameView(this, board);
		setContentView(view);
	}
}
