package com.example.game;

import java.util.ArrayList;

import android.R.string;
import android.graphics.Point;
import android.graphics.Rect;
/*
1		0	0	2	0	0		3
0		4	0	5	0	6       0
0		0	7	8	9   0       0
10		11	12	-1	13	14		15
0		0	16	17	18  0		0
0		19	0	20	0	21      0
22		0	0	23	0	0		24		
Neighbours format (left,up,right,down) 
Neighbours map is:  left up right down
 */
public class Cell {
	public int player_id ;
	public int []neighbours={0,0,0,0};
	public int current_balata;
	public int cellId;
	public Point size;
	public Rect position;
	public Cell() {
		player_id = 0;
		current_balata = -1;
	}
	public Boolean CouldMoveTo(int id){
		return true;
		
	}
	public void generatePosition(Point size){
		
	}
	public Boolean inRange(Point pos){
		return position.contains(pos.x,pos.y);
	}
}
