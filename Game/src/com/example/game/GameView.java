package com.example.game;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

public class GameView extends View {

	private ImageButton MainNewGame;
	private final int UP = 0, DOWN = 1, LEFT = 2, RIGHT = 3;
	private Board board;
	private Boolean current_player;
	private Player[] p = new Player[2];
	private int selectedId;
	private int whoPlay; // 0=player 1 , 1 =player 2
	private Paint line, bg, sel;
	private int horSX[], horEX[], horY[], width, height, nei[][];
	private int fromID = -1, toID = -1, coinID = -1, selCell = 0 , eatID = -1;
	// Example to draw image
	Drawable m, bigSqaureDraw, middleSquareDraw, smallSquareDraw, horBarDraw,
			verBarDraw, cellDraw;
	Drawable cellSelDraw;
	private int d = 15;
	private Rect[] rect;

	public GameView(Context context, Board b) {
		super(context);
		m = context.getResources().getDrawable(R.drawable.coin1);
		bigSqaureDraw = context.getResources().getDrawable(
				R.drawable.bigest_square);
		middleSquareDraw = context.getResources().getDrawable(
				R.drawable.middle_squra);
		smallSquareDraw = context.getResources().getDrawable(
				R.drawable.small_square);
		horBarDraw = context.getResources().getDrawable(
				R.drawable.horizontal_bar);
		verBarDraw = context.getResources()
				.getDrawable(R.drawable.vertical_bar);
		cellDraw = context.getResources().getDrawable(R.drawable.circle);
		cellSelDraw = context.getResources().getDrawable(
				R.drawable.circle_selected);

		selectedId = 0;
		whoPlay = 0;
		board = b;
		line = new Paint();
		line.setColor(Color.RED);
		sel = new Paint();
		sel.setColor(Color.CYAN);
		bg = new Paint();
		bg.setColor(Color.rgb(47, 137, 204));
		horSX = b.getHorSX();
		horEX = b.getHorEX();
		horY = b.getHorY();
		nei = b.getNeighbours();
		rect = b.getRect();
		bigSqaureDraw.setBounds(horSX[0], horY[0], horEX[0], horY[7]);
		middleSquareDraw.setBounds(horSX[1], horY[1], horEX[1], horY[6]);
		smallSquareDraw.setBounds(horSX[2], horY[2], horEX[2], horY[5]);

		p[0] = new Player(b);
		p[0].setDrawable(context.getResources().getDrawable(R.drawable.coin1));
		p[1] = new Player(b);
		p[1].setDrawable(context.getResources().getDrawable(R.drawable.coin2));

		setFocusable(true);
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		width = w;
		height = h;
		if (w != 480) {
			float rate = 480 / (float) w;
			float dd = (rate < 1) ? 15 * rate : -15 / rate;
			d += (int) dd;
		}

		super.onSizeChanged(w, h, oldw, oldh);
	}

	// Updating Board with drawable contents
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		canvas.drawRect(0, 0, width, height, bg);

		bigSqaureDraw.draw(canvas);
		middleSquareDraw.draw(canvas);
		smallSquareDraw.draw(canvas);
		horBarDraw.setBounds(rect[9].left + d / 4, rect[9].top + d / 4,
				rect[11].left, rect[9].bottom - d / 4);
		horBarDraw.draw(canvas);
		horBarDraw.setBounds(rect[12].left + d / 4, rect[12].top + d / 4,
				rect[14].right, rect[12].bottom - d / 4);
		horBarDraw.draw(canvas);
		verBarDraw.setBounds(rect[1].left + d / 4, rect[1].top + d / 2,
				rect[1].left + d / 2 + d, rect[7].top + d / 2);
		verBarDraw.draw(canvas);
		verBarDraw.setBounds(rect[16].left + d / 4, rect[16].top + d / 2,
				rect[16].left + d / 2 + d, rect[22].top + d / 2);
		verBarDraw.draw(canvas);
		for (int i = 0; i < rect.length; i++) {
			cellDraw.setBounds(rect[i]);
			cellDraw.draw(canvas);
		}
		if (fromID != -1) {
			cellSelDraw.setBounds(rect[fromID]);
			cellSelDraw.draw(canvas);
		}
		cellSelDraw.setBounds(rect[selCell]);
		cellSelDraw.draw(canvas);
		p[0].drawCoins(canvas, rect);
		p[1].drawCoins(canvas, rect);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_W:
		case KeyEvent.KEYCODE_DPAD_UP:
			select(nei[selCell][UP]);
			break;
		case KeyEvent.KEYCODE_S:
		case KeyEvent.KEYCODE_DPAD_DOWN:
			select(nei[selCell][DOWN]);
			break;
		case KeyEvent.KEYCODE_D:
		case KeyEvent.KEYCODE_DPAD_RIGHT:
			select(nei[selCell][RIGHT]);
			
			break;
		case KeyEvent.KEYCODE_A:
		case KeyEvent.KEYCODE_DPAD_LEFT:
			select(nei[selCell][LEFT]);
			break;
		case KeyEvent.KEYCODE_SPACE:
		case KeyEvent.KEYCODE_DPAD_CENTER:
			operationsOnSelected();
			break;
		default:
			return super.onKeyDown(keyCode, event);
		}
		invalidate();
		//	if (gameComplete()) {
		// finishGame();
		// }
		return true;

	}

	//@Override
	public boolean onTouchEvent(MotionEvent event) {
		 Point touchPosition = new Point((int) event.getX(), (int) event.getY());
		 int id=board.getId(touchPosition);
		 if(id>=0 && id<=23)
		 {
			 switch (event.getAction()) {
			 case MotionEvent.ACTION_UP:
			 	manageSelectMove(id);
			 	break;
			 }
		 }
		 invalidate();
		 return true;
	 }
	private void select(int id) {
		selCell = id;
	}

	private void unSelect() {
		selCell = -1;
	}

	private void manageSelectMove(int touchedId) {
		if(selCell==touchedId)
		{
			operationsOnSelected();
		}
		else
		{
			select(touchedId);
		}
	 }
	private void operationsOnSelected(){
		if(eatID == -2){
			if(p[(whoPlay+1)%2].findCellInCoins(selCell) != -1){
				nextPlayer();
				p[whoPlay].clearCell(selCell);
				eatID = -1;
			}
		}
		else if (p[whoPlay].finishedPush() == false) {
			if (p[0].findCellInCoins(selCell) == -1 && p[1].findCellInCoins(selCell) == -1) {
				p[whoPlay].pushCoin(selCell);
				if(p[whoPlay].canEat(selCell))
					eatID = -2;
				else
					nextPlayer();
			}
		} else if (fromID == -1) {
			if (p[whoPlay].findCellInCoins(selCell) != -1)
				fromID = selCell;
		} else {//move
			if (p[0].findCellInCoins(selCell) == -1 && p[1].findCellInCoins(selCell) == -1) {
				if(p[whoPlay].moveCoin(fromID, selCell)){
					fromID = -1;
					if(p[whoPlay].canEat(selCell))
						eatID = -2;
					else
						nextPlayer();
				}
			}
			else
				fromID = -1;
		}
	}
	private void nextPlayer() {
		whoPlay++;
		whoPlay %= 2;
	}
}
